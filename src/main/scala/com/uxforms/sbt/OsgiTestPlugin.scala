package com.uxforms.sbt

import sbt.Keys._
import sbt._

object OsgiTestPlugin extends AutoPlugin {

  object autoImport {
    lazy val OsgiTest = config("osgiTest").extend(Test)
  }

  import autoImport._

  override def requires: Plugins = plugins.JvmPlugin

  override def trigger: PluginTrigger = allRequirements

  override def projectConfigurations: Seq[Configuration] = Seq(OsgiTest)

  override def projectSettings: Seq[Setting[_]] = inConfig(OsgiTest)(baseOsgiTestSettings) ++ Seq(
    libraryDependencies ++= {
      lazy val paxExamVersion = "4.6.0"
      Seq(
        "com.uxforms" %% "test" % "0.6" % OsgiTest,
        "org.ops4j.pax.exam" % "pax-exam-container-native" % paxExamVersion % OsgiTest,
        "org.ops4j.pax.exam" % "pax-exam-junit4" % paxExamVersion % OsgiTest,
        "org.ops4j.pax.exam" % "pax-exam-link-mvn" % paxExamVersion % OsgiTest,
        "org.ops4j.pax.url" % "pax-url-aether" % "2.4.3" % OsgiTest,
        "org.apache.felix" % "org.apache.felix.framework" % "5.4.0" % OsgiTest
      )
    }
  )

  lazy val baseOsgiTestSettings: Seq[Setting[_]] =
    Defaults.testSettings ++
      Seq(
        sourceDirectory := baseDirectory.value / "src" / "osgi",
        parallelExecution := false,
        internalDependencyClasspath := (internalDependencyClasspath in Test).value
      )


}