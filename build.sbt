sbtPlugin := true

organization := "com.uxforms"

name := "test-plugin"

scalaVersion := "2.10.5"

scalacOptions ++= Seq("-deprecation", "-feature")

shellPrompt := { state => "[" + scala.Console.CYAN + name.value + scala.Console.RESET + "] $ " }

bintrayOrganization := Some("equalexperts")

bintrayRepository := "uxforms-sbtplugins"

publishMavenStyle := false

licenses += ("MIT", url("http://opensource.org/licenses/MIT"))

resolvers ++= Seq(
  Resolver.bintrayRepo("equalexperts", "uxforms-releases")
)
