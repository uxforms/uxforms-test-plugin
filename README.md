# test-plugin

An sbt plugin for easily testing components against a UX Forms-compatible OSGi container.

## Usage

If you are already using UX Forms' `formdefinition-sbt-plugin` then this plugin is already imported into your project.

Otherwise, for sbt `0.13.5` and above, add a new file called `osgitest.sbt` in `project/` with the contents:
```
resolvers += Resolver.url("uxforms-sbtplugins", url("https://dl.bintray.com/equalexperts/uxforms-sbtplugins"))(Resolver.ivyStylePatterns)
addSbtPlugin("com.uxforms" % "test-plugin" % "latest.integration")
```

Reload your project and you're good to go.

## Testing your form definition

This plugin adds a new configuration, `osgiTest`.

Its sources must be in `src/osgi/scala`.

Here is a sample test that spins up a UX Forms-compatible OSGi container and deploys a form definition in to it.

A bundle with the symbolic name `my-example-form` is injected into the test instance if it was successfully
deployed in to the OSGi container.

```
package com.example


import java.util.Locale
import javax.inject.Inject

import com.example.BuildInfo
import com.uxforms.domain.FormDefinitionFactory
import com.uxforms.test.OsgiContainer
import org.junit.Test
import org.junit.runner.RunWith
import org.ops4j.pax.exam.CoreOptions._
import org.ops4j.pax.exam.Option
import org.ops4j.pax.exam.junit.PaxExam
import org.ops4j.pax.exam.spi.reactors.{ExamReactorStrategy, PerSuite}
import org.ops4j.pax.exam.util.{Filter, PathUtils}

import org.hamcrest.MatcherAssert._
import org.hamcrest.CoreMatchers._

@RunWith(classOf[PaxExam])
@ExamReactorStrategy(Array(classOf[PerSuite]))
class OsgiContainerTest extends OsgiContainer {

  override val projectArtifact: Option = composite(
    // Add your form definition to the container
    bundle(BuildInfo.artifact.toURI.toURL.toExternalForm),
    // Add any extra configuration or libraries over and above the base UX Forms configuration that your form definition requires
    systemProperty("logback.configurationFile").value(s"file:${PathUtils.getBaseDir}/src/osgi/resources/logback.xml")
  )

  @Inject
  @Filter("Bundle-SymbolicName=my-example-form")
  var formDef: FormDefinitionFactory = _

  @Test
  def formDefinitionFactoryIsDeployed(): Unit = {
    assertThat(formDef, is(notNullValue))
    assertThat(formDef.formDefinition(new Locale("en", "GB")).name.toString(), is(equalTo("my-example-form")))
  }
}

```

`OsgiContainer` uses [Pax Exam](https://ops4j1.jira.com/wiki/display/PAXEXAM4/Pax+Exam) to provision an OSGi container, populated
 with all of the libraries provided by UX Forms. Have a look at Pax Exam's [configuration options](https://ops4j1.jira.com/wiki/display/PAXEXAM4/Configuration+Options)
 to see how additional libraries and configuration options can be applied to the container.

This example makes use of the excellent [BuildInfo](https://github.com/sbt/sbt-buildinfo) plugin to reference the build artifact, but
you're free to do this in whatever way suits your project best. For reference, this is how to add and configure the `BuildInfo` plugin to make use of it as above:

Add the following in to `/project/buildinfo.sbt`: `addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.5.0")`

And in your `build.sbt`:
```
lazy val root = (project in file('.'))
    .enablePlugins(BuildInfoPlugin)
    .settings(
        buildInfoPackage := "com.example.build" // can be anything, but it's nice to keep generated sources separate
        buildInfoKeys ++= Seq[BuildInfoKey](
            "artifact" -> (artifactPath in (Compile, packageBin)).value
        )
    )
```

## Running the OSGi container tests

Execute your OSGi container test(s) with the following:

```
sbt osgiTest:test
```

If you are using this plugin in the same project as your form definition, you may want to ensure that your form definition's
osgi bundle is created before the OSGi container tests are run. You can do this by adding the following to your project's `build.sbt`:

```
(test in OsgiTest) <<= (test in OsgiTest).dependsOn(OsgiKeys.bundle)
```

You also can do the same with the `testOnly` and `testQuick` tasks if you wish.

### Notes

As these tests start up an OSGi container, parallel test execution is disabled within osgiTest's configuration. This does not affect
the configuration of your 'normal' tests in `src/test`.